#ifndef CREATEIMAGE_H
#define CREATEIMAGE_H
#include <string>
#include <vector>
#include <QPicture>
#include <QPainter>
#include <QImage>
#include <math.h>
#include "colormap.h"

void calcDimensions(const std::vector<std::vector<float> >& bins, float window_width, float window_height,
                    float* bin_width, float* bin_height, float* xr, float* yr)
{
    float domain_width = bins[bins.size()-1][1] - bins[0][0];
    float domain_height = bins[bins.size()-1][3] - bins[0][2];

    *xr = window_width/domain_width;
    *yr = window_height/domain_height;

    *bin_width = (*xr)*(bins[0][1] - bins[0][0]);
    *bin_height = (*yr)*(bins[0][3] - bins[0][2]);
}

void drawBinMap(std::string imgpath, const std::vector<std::vector<float> >& bins, std::vector<float>& arr,
                float window_width, float window_height)
{
    float bin_width, bin_height, xr, yr;

    calcDimensions(bins, window_width, window_height,
                   &bin_width, &bin_height, &xr, &yr);
    QPainter p;
    QImage img(window_width, window_height, QImage::Format_ARGB32);
    p.begin(&img);
    p.setRenderHint(QPainter::Antialiasing);
    QColor black(0,0,0);
    p.fillRect(0,0,window_width,window_height,black);
    CMap cmap("ytor");
    std::vector<float> norm = cmap.linearNorm(arr);
    int rgb[3] = {0,0,0};

    for(unsigned int i=0; i<bins.size(); i++) {
        cmap.getRGB(norm[i], rgb);
        QColor c(rgb[0], rgb[1], rgb[2]);
        float x = xr*bins[i][0];
        float y = window_height-yr*bins[i][2];
        p.fillRect(floor(x),floor(y),ceil(bin_width),ceil(bin_height),c);
    }
    p.end();

    img.save(imgpath.c_str());
}

void drawArrow(QPainter* p, float x0, float y0,float x1,
               float y1,float L,float w,float alpha, QColor c)
{
    float t = w/(2*(tan(alpha)/2) * L);
    float xb = x1 - t * (x1-x0);
    float yb = y1 - t * (y1-y0);

    float nx = y1 - y0;
    float ny = -1 * (x1 - x0);
    float nt;
    if(L > 0)
        nt = w/(2*L);
    else
        return;

    float lpx = xb + nt*nx;
    float lpy = yb + nt*ny;
    float rpx = xb - nt*nx;
    float rpy = yb - nt*ny;

    p->setPen(QPen(c, 2, Qt::SolidLine, Qt::RoundCap));
    p->drawLine(x0,y0,x1,y1);
    p->drawLine(x1,y1,rpx,rpy);
    p->drawLine(x1,y1,lpx,lpy);
}

void createVelocityImage(std::string folder, const std::vector<std::vector<float> >& bins,
                         float window_width, float window_height)
{
    std::cout << "Creating velocity image.\n";
    std::string imgpath = folder + "/velocity.png";

    std::vector<float> vel(bins.size());
    for(unsigned int i=0; i<bins.size(); i++) {
        float vx = bins[i][4];
        float vy = bins[i][5];
        float vz = bins[i][6];
        vel[i] = sqrt(vx*vx + vy*vy + vz*vz);
    }

    drawBinMap(imgpath, bins, vel, window_width, window_height);
}

void createSpeedImage(std::string folder, const std::vector<std::vector<float> >& bins,
                      float window_width, float window_height)
{
    std::cout << "Creating speed image.\n";
    std::string imgpath = folder + "/speed.png";

    std::vector<float> speed(bins.size());
    for(unsigned int i=0; i<bins.size(); i++)
        speed[i] = bins[i][7];

    drawBinMap(imgpath, bins, speed, window_width, window_height);
}

void createPopulationImage(std::string folder, const std::vector<std::vector<float> >& bins,
                           float window_width, float window_height)
{
    std::cout << "Creating speed image.\n";
    std::string imgpath = folder + "/population.png";

    std::vector<float> population(bins.size());
    for(unsigned int i=0; i<bins.size(); i++)
        population[i] = bins[i][8];

    drawBinMap(imgpath, bins, population, window_width, window_height);
}

void createArrowsImage(std::string folder, const std::vector<std::vector<float> >& bins,
                       float window_width, float window_height)
{
    std::cout << "Creating arrows image.\n";
    std::string imgpath = folder + "/arrows.png";
    float bin_width, bin_height, xr, yr;
    calcDimensions(bins, window_width, window_height,
                   &bin_width, &bin_height, &xr, &yr);

    //std::vector<float> speed(bins.size());
    //for(unsigned int i=0; i<bins.size(); i++)
        //speed[i] = bins[i][7];

    QPainter p;
    QImage img(window_width, window_height, QImage::Format_ARGB32);
    p.begin(&img);
    p.setRenderHint(QPainter::Antialiasing);
    QColor c(0, 0, 0);
    p.fillRect(0,0,window_width,window_width,c);
    // Calculate arrow properties
    float x0,y0,x1,y1,theta,L,w,alpha,vx,vy;
    L = (bin_width+bin_height)*0.4;
    w = L/2;
    alpha = M_PI/3;

    CMap cmap("ytor");
    std::vector<float> vel(bins.size());
    for(unsigned int i=0; i<bins.size(); i++) {
        float vx = bins[i][4];
        float vy = bins[i][5];
        float vz = bins[i][6];
        vel[i] = sqrt(vx*vx + vy*vy + vz*vz);
    }
    std::vector<float> norm = cmap.linearNorm(vel);
    int rgb[3] = {0,0,0};

    for(unsigned int b=0; b<bins.size(); b++) {
        cmap.getRGB(norm[b], rgb);
        QColor arrow_c(rgb[0], rgb[1], rgb[2]);
        x0 = xr*(bins[b][1] + bins[b][0])/2;
        y0 = yr*(bins[b][3] + bins[b][2])/2;
        vx = bins[b][4];
        vy = bins[b][5];
        if(vx == 0) {
            theta = M_PI/2;
        } else {
            theta = atan(vy/vx);
        }
        x1 = x0 + L*cos(theta);
        y1 = (y0 + L*sin(theta));
        drawArrow(&p,x0,window_height-y0,x1,window_height-y1,L,w,alpha,arrow_c);
    }
    p.end();

    img.save(imgpath.c_str());
}


#endif // CREATEIMAGE_H
