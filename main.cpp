#include <QApplication>
#include <QLabel>
#include <QPicture>
#include <QPainter>
#include <QImage>
#include <vector>
#include <string>
#include "colormap.h"
#include "csv.h"
#include <math.h>
#include "createimage.h"

using namespace std;

int main(/*int argc, char *argv[]*/)
{
    string filepath;
    cout << "Enter the path to the .csv file containing the binned data.\n";
    cin >> filepath;
    CSVFile infile(filepath);
    infile.open('r');
    vector<vector<float> > bins = infile.readAll();
    infile.close();


    float window_width = 1200;
    float window_height = 410;

    string imgfolder;
    cout << "Enter the path of the folder to contain the images.\n";
    cin >> imgfolder;

    createVelocityImage(imgfolder, bins, window_width, window_height);
    createSpeedImage(imgfolder, bins, window_width, window_height);
    createPopulationImage(imgfolder, bins, window_width, window_height);
    createArrowsImage(imgfolder, bins, window_width, window_height);


    cout << "Complete" << endl;
}
