#include "colormap.h"

void CMap::sRainbow(float f, int rgb[3])
{
    float a = (1.0-f)/0.25;
    int X = floor(a);
    int Y = floor(255*(a-X));

    switch(X)
    {
        case 0: rgb[0]=255; rgb[1]=Y; rgb[2]=0;break;
        case 1: rgb[0]=255-Y; rgb[1]=255; rgb[2]=0;break;
        case 2: rgb[0]=0; rgb[1]=255; rgb[2]=Y;break;
        case 3: rgb[0]=0; rgb[1]=255-Y; rgb[2]=255;break;
        case 4: rgb[0]=0; rgb[1]=0; rgb[2]=255;break;
    }
}

void CMap::grayscale(float f, int rgb[3])
{
    for(int i=0; i<3; i++) {
        rgb[i] = (int)(f*255);
    }
}

void CMap::ytor(float f, int rgb[3])
{
    float a = (1-f);
    rgb[0] = 255;
    rgb[1] = floor(255*a);
    rgb[2] = 0;
}


void CMap::getRGB(float f, int rgb[3])
{
    (this->*mapfunc)(f, rgb);
}


void CMap::initTypeMap()
{
    type_dict = {
        {"srainbow", 0},
        {"grayscale", 1},
        {"ytor",2}
    };
}
