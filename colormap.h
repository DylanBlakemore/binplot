#ifndef COLORMAP_H
#define COLORMAP_H
#include <vector>
#include <string>
#include <math.h>
#include <map>
#include <algorithm>
#include <iostream>


class CMap {
public:
    CMap(std::string t)
    {
        stype = t;
        initTypeMap();
        if(type_dict.count(stype) == 0) {
            itype = 0;
        } else {
            itype = type_dict[stype];
        }
        switch(itype)
        {
            case 0: mapfunc=&CMap::sRainbow;break;
            case 1: mapfunc=&CMap::grayscale;break;
            case 2: mapfunc=&CMap::ytor;break;
        }
    }

    void getRGB(float f, int rgb[3]);

    std::vector<float> linearNorm(std::vector<float>& in, float min, float max)
    {
        std::vector<float> normal(in.size());
        for(unsigned int i=0; i<in.size(); i++) {
            float norm = (in[i] - min)/(max - min);
            if(norm < 0)norm=0;
            if(norm > 1)norm=1;
            normal[i] = norm;
        }
        return normal;
    }

    std::vector<float> linearNorm(std::vector<float>& in)
    {
        std::vector<float>::iterator max_it;
        std::vector<float>::iterator min_it;

        max_it = std::max_element(in.begin(), in.end());
        min_it = std::min_element(in.begin(), in.end());
        return linearNorm(in, *min_it, *max_it);
    }

    std::vector<float> logNorm(std::vector<float>& in, float min, float max)
    {
        std::vector<float> logv(in.size());
        std::vector<float>::iterator min_it = std::min_element(in.begin(), in.end());

        for(unsigned int i=0; i<in.size(); i++) {
            float tmp = in[i] - *min_it + 1;
            if(in[i] < min)tmp = min - *min_it + 1;
            if(in[i] > max)tmp = max - *min_it + 1;
            logv[i] = log(tmp);
        }
        return linearNorm(logv);
    }

    std::vector<float> logNorm(std::vector<float>& in)
    {
        std::vector<float>::iterator max_it;
        std::vector<float>::iterator min_it;

        max_it = std::max_element(in.begin(), in.end());
        min_it = std::min_element(in.begin(), in.end());
        return logNorm(in, *min_it, *max_it);
    }

    std::vector<float> expNorm(std::vector<float>& in)
    {
        std::vector<float> linnorm = linearNorm(in);
        std::vector<float> expnorm(linnorm.size());
        for(unsigned int i=0; i<linnorm.size(); i++) {
            expnorm[i] = pow(2, linnorm[i]) - 1;
        }
        return expnorm;
    }


private:
    typedef void (CMap::*mptr)(float, int[3]);
    void initTypeMap();
    void sRainbow(float f, int rgb[3]);
    void grayscale(float f, int rgb[3]);
    void ytor(float f, int rgb[3]);

    std::map<std::string, int> type_dict;
    std::string stype;
    int itype;
    mptr mapfunc;
};

#endif // COLORMAP_H
